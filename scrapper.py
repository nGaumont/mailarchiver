import sys  # needed in order to read the command line argument
from bs4 import BeautifulSoup  # query the DOM
import requests  # get an url.
from urllib.parse import urljoin
import re  # regexp
from time import sleep
from dateutil import parser
import date


class Mail:
    """Store everything needed for a mail"""
    def __init__(self, time=0, message_id='', auth='', answer='', thread_id=''):
        self.author = auth
        self.id = message_id
        self.ans = answer
        self.t = time
        self.thread = thread_id
        self.ref = ""
        self.page = ""

    def identifiable(self):
        """ make sure some fields (except thread) are not empty """
        return (self.t != 0 and self.author != "" and self.id != "")

    def dump(self):
        return ";".join(map(lambda x: str(x), [self.t, self.id, self.ans,
                                               self.author, self.page,
                                               self.ref]))


def usage():
    print("""This script is used to fetch the meta-data of a mailling list.
You can us it this way:
    python scrapper.py URL outpufile
or to show this message:
    python scrapper.py -h
    """)
    exit(1)


def get_Month_Url(doc, url, min_date=0):
    """Get all the monthly link if its year > min_date and return them.

    Keyword arguments:
    doc -- The main page which lists all the month.
    url -- base url used to make link absolute.
    min_date -- The minimum year to fetch a month such as 2002.
    """
    res = []
    # Get all the links which are in list wich is in a class "index_include"
    all_year = soup.select(".index_include li")
    for year in all_year:
        date = int(year.getText().split(' ')[0])
        if date >= min_date.year:
            months = year.select('a')
            for m in months:
                cur_date = parser.parse("01 "+m.getText()+" "+str(date))
                if cur_date >= min_date:
                    res.append((cur_date, urljoin(url, m["href"])))
    return res


def nextPage(cur_page, url):
    nav = cur_page.select("tr td a")
    for i in nav:
        if "next page" in i.getText():
            res = urljoin(url, i["href"])
            return res
    return False

regexp = re.compile("([a-zA-Z0-9\._%$!/\"&~+(){}=\-\*]+@[a-zA-Z0-9\.\-\[\]]+)")


def extract_id(string):
    """Extract something that look like email adress"""
    res = regexp.findall(string)
    if len(res) == 1:
        return res[0]
    return False


def extract_ref(string):
    """Extract something that look like email adresses"""
    res = regexp.findall(string)
    return res


def scrapp_Mail(page, cur_mail):
    meta_data = page.select("body > ul > li > em")
    well_structured = True
    mail = Mail()
    mail.page = cur_mail
    for m in meta_data:
        text = m.getText()
        splitted = m.parent.getText().split(':', 1)
        if len(splitted) != 2:
            continue
        content = splitted[1].strip()
        # print(text, ":"+content)
        if text in "From":
            extract = extract_id(content)
            if extract:
                mail.author = extract
        elif text in "Message-id":
            extract = extract_id(content)
            if extract:
                mail.id = extract
        elif text in "In-reply-to":
            extract = extract_id(content)
            if extract:
                mail.ans = extract
        elif text in "References":
            extract = extract_ref(content)
            mail.ref = extract
        elif text in "Date":
            try:
                mail.t = date.parse(content)
            except:
                mail.t = 'UNKNOWN_'+content
            # print("Time:", mail.t)

    if mail.identifiable():  # It only make sure we have something
        return mail
    return False


def scrapp_Page(page, cur_month, url, fail):
    mails = page.select("strong a")
    for mail in mails:
        cur_mail = urljoin(url, mail["href"])
        page = BeautifulSoup(requests.get(cur_mail).text)
        print("Cur_page:", cur_mail)
        m = scrapp_Mail(page, cur_mail)
        if m:
            cur_month.append(m.dump()+'\n')
        else:
            fail.append(cur_mail)  # Fail to retreive mail from this page
        sleep(0.1)


def fetch_Month(link_month):
    page_link = link_month[1]
    cur_month = []
    fails = []
    print(link_month[0].date())
    while(page_link):
        doc = requests.get(page_link)
        page = BeautifulSoup(doc.text)
        scrapp_Page(page, cur_month, page_link, fails)
        page_link = nextPage(page, page_link)
    with open("Res/"+str(link_month[0].date())+".txt", "w") as output:
        for mail in cur_month:
            output.write(mail)
    if len(fails) != 0:
        with open("FAIL.txt", "a") as output:
            for fail in fails:
                output.write(fail)

if __name__ == "__main__":
    if "-h" in sys.argv or '--help' in sys.argv or len(sys.argv) != 2:
        usage()

    url = sys.argv[1]
    resultat = requests.get(url)
    soup = BeautifulSoup(resultat.text)
    months = get_Month_Url(soup, url, parser.parse("01 Jan 1996"))
    for m in months:
        fetch_Month(m)
