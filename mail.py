# -*- coding:utf-8
import gzip
import sys
import os
import functools
import re
# import matplotlib.pyplot as plt
import numpy as np

# Output file format, list of:
#	T Author1 Author2 Answerdelay Root_id
#  	
#	where T is the time at which Author1 answerd an email from Author2 in the thread Root_id.
#	It tooks Answerdelay seconds to answer.
#
#	This scripts also discards a lot mails for various:
#		- Missing informations
#		- Having a mail answering a missing mail in the dataset 
#		- Mails having a timetamps before 1996
#		- Answer before the email
#		- Mail where author1 == author2
#		- Missing the first mail in the thread
#		- Thread that begins near the end of capture (4 years before the ends) because we don't know wehter the thread is finished or not.
#		- Thread that last more than 4 years
#	This for 4 years limits has been by looking at the thread duration distribution.
#
#	If a thread is borken  because one its mail is discarded, then the entire thread is removed.
#	Also, the first mail in the thread is not shown in the output.


# Inputfile format :
# t;e1;e2;a1;url
# où e1 et e2 sont des identifiants d'emails tels que e1 est une
# réponse à e2 (e2 est vide si c'est un email qui n'est une réponse à
# personne),
# où t est le timestamp de e1,
# où a1 est l'auteur de e1.


# Il faudrait vraiment faire de l'objet pour la liste des mails

if len(sys.argv) < 3 or len(sys.argv) > 3:
    print('Fail, 2 arguments needed:')
    print('\texample: python mail.py PathSourceFile PathOutputFile')
    exit(0)

msgDict = dict()
senderToId = dict()
senderCounter = 0
rootCounter = 0
rootId = dict()
lineCounter = 0
linkCounter = 0
previousTime = 0
doublon = 0

msg_Author = dict()  # dict msg <-> author.
thread_update = []
END_TIME = 0
with open(sys.argv[1], 'r') as inFile:
    for line in inFile:
        tab = line.split(";")
        lineCounter += 1
        if "UNKNOWN" in tab[0]:
            continue
        curTime = int(float(tab[0]))

        if len(tab) < 5:
            print("probleme with the line")
            print(lineCounter, tab)
            exit(-1)

        e1 = tab[1]
        if tab[2] == "":
            e2 = e1  # e1 réponds à
            thread = e1
        else:
            e2 = tab[2]
            thread = ""
        a1 = tab[3]
        url = tab[4]
        if e1 == "" or a1 == "":
            # print("Line ignored because the message id or the author could\
            #  not be retrieved")
            continue
        END_TIME = max(END_TIME, curTime)
        ligne = {"time": curTime, "e1": e1, "e2": e2, "a1": a1,
                 "thread": thread, "url": url, "line": lineCounter}
        if e1 in msgDict:
            if msgDict[e1]["e2"] != ligne["e2"] or msgDict[e1]["a1"] != ligne["a1"] or msgDict[e1]["thread"] != ligne["thread"] or msgDict[e1]["time"] != ligne["time"] or msgDict[e1]["url"] != ligne["url"]:
                # print("Vrai doublon ", ligne)
                # print(msgDict[e1])
                doublon += 1
            else:
                msgDict[e1]["time"] = -1  # the link will deleted after
                continue
        else:
            msgDict[e1] = ligne

        if e2 in msgDict:
            msgDict[e1]["thread"] = msgDict[e2]["thread"]
        else:
            thread_update.append(e1)
        linkCounter += 1

        msg_Author[e1] = a1
        if a1 not in senderToId:
            senderToId[a1] = senderCounter
            senderCounter += 1

        if thread not in rootId:
            rootId[thread] = rootCounter
            rootCounter += 1
            # print(ligne)
            # input()
for m in thread_update:
    if msgDict[m]["thread"] != "":
        continue  # The msg could have been updated.
    if msgDict[m]["e2"] not in msgDict:
        continue  # We don't have the parent.
    parent = msgDict[msgDict[m]["e2"]]
    # go back in time to find the first mail with the information.
    to_update = [m]
    fail = False
    while parent["thread"] == "":
        if parent["e2"] in msgDict:
            to_update.append(parent["e1"])
            parent = msgDict[parent["e2"]]
        else:
            fail = True
            break
    if fail:
        continue
    for mail_to_update in to_update:
        msgDict[mail_to_update]["thread"] = parent["thread"]


print("Récuparation terminée")
print("Lignes ignorées: ", lineCounter - linkCounter, "doublon", doublon)

erreur = {"MissPreviousMsg": 0, "IncoherentOrder": 0, "rootbegin": 0,
          "NegativeTime": 0, "self-loop": 0, "Pb_Multiple_Author": 0,
          "rootmiss": 0, "biased_correction": 0}
min_t = 999999999999999999999999
max_t = 0
# data = []
MessageToRemove = []
threadToRemove = set()
for m in msgDict:
    e2 = msgDict[m]["e2"]
    t = msgDict[m]["time"]

    if t < (26 * 365.25 * 3600 * 24):  # 1st january 1996
        erreur["NegativeTime"] += 1
        MessageToRemove.append(m)
        threadToRemove.add(msgDict[m]["thread"])
        continue

    if t < min_t:
        min_t = t
        l_min = msgDict[m]
    max_t = t if t > max_t else max_t
    if e2 not in msgDict:
        erreur["MissPreviousMsg"] += 1
        MessageToRemove.append(m)
        threadToRemove.add(msgDict[m]["thread"])
        continue
    if msgDict[m]["thread"] == "":
        erreur["rootmiss"] += 1
        MessageToRemove.append(m)
        threadToRemove.add(msgDict[m]["thread"])
        continue
    thread_t = msgDict[msgDict[m]["thread"]]["time"]
    if e2 == msgDict[m]["e1"] and msgDict[m]["e1"] == msgDict[m]["thread"]:
        erreur["rootbegin"] += 1
        if (END_TIME - thread_t) < (4 * 365.25 * 3600 * 24):
            MessageToRemove.append(m)
            threadToRemove.add(m)
            erreur["biased_correction"] += 1
        continue
    if msgDict[e2]["time"] > msgDict[m]["time"]:
        erreur["IncoherentOrder"] += 1
        MessageToRemove.append(m)
        threadToRemove.add(msgDict[m]["thread"])
        continue

    if msgDict[m]["a1"] == msgDict[e2]["a1"]:
        erreur["self-loop"] += 1
        MessageToRemove.append(m)
        threadToRemove.add(msgDict[m]["thread"])
        continue;

    if (t - thread_t) > (4 * 365.25 * 3600 * 24):
        erreur["biased_correction"] += 1
        MessageToRemove.append(m)
        threadToRemove.add(msgDict[m]["thread"])
        continue;

    # wait = input("PRESS ENTER TO CONTINUE.")
print(erreur)
print("Il y a ", len(MessageToRemove), " qui pose pb.")

msgDict = {m:msgDict[m] for m in msgDict if msgDict[m]["thread"] not in threadToRemove}

for m in MessageToRemove:
    if m in msgDict:
        del msgDict[m]


MessageToRemove = []
allgood = False
cascade = 0
rootId = dict()
conversation = 0
for m in msgDict:
    e2 = msgDict[m]["e2"]
    if e2 == msgDict[m]["e1"] and msgDict[m]["e1"] == msgDict[m]["thread"]:
        rootId[m] = conversation
        conversation += 1
while not allgood:
    allgood = True
    for m in msgDict:
        e2 = msgDict[m]["e2"]
        if e2 not in msgDict or msgDict[m]["thread"] not in rootId:
            MessageToRemove.append(m)
            cascade += 1
            allgood = False
    for m in MessageToRemove:
        del msgDict[m]
    # print("taille cascade courante:",cascade)
    # input()
    MessageToRemove = []
dif = 0
difRoot = dict()
for m in msgDict:
    e2 = msgDict[m]["e2"]
    if not(e2 == msgDict[m]["e1"] and
       msgDict[m]["e1"] == msgDict[m]["thread"]):
        difRoot[msgDict[m]["thread"]] = True


print("Nombre de conversations :", len(rootId), "dont",
      len(difRoot), "qui ont plus d'un message.")
print("Nombre de msg envoye dans l'archive (filtré de certaine incohérence) :",
      len(msgDict))


with open(sys.argv[2], 'w') as outFile:
    for m in msgDict:
        if msgDict[m]["e2"] == msgDict[m]["e1"] and msgDict[m]["e1"] == msgDict[m]["thread"]:
            continue
        e2 = msgDict[m]["e2"]
        a1 = senderToId[msgDict[m]["a1"]]
        a2 = senderToId[msgDict[e2]["a1"]]
        t = msgDict[m]["time"]
        d = t - msgDict[e2]["time"]
        if(d < 0):
            print(msgDict[m])
            print(msgDict[e2])
        assert(d >= 0)
        ligne = (t - min_t, a1, a2, d, rootId[msgDict[m]["thread"]])

        outFile.write(' '.join(map(str, ligne)) + '\n')
# outFile.close()
