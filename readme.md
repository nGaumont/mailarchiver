MailArchiver
=========================
This program scrapp a given website which contains an archive from mailman and try to extract the meta data.  
In output, each line has the following format:
`
t;e1;e2;a1;url
`

wich means that the message *e1* answers the message *e2* at time *t*.  
The message *e1* is written by *a1*.  
The message *e1* has been scrapp from *url*

*e1* is considered as the first mail in the thread if e2 is empty

# Usage #

`
python scrapper.py https://lists.debian.org/debian-user/
`

In output, a file is created for each month wich has been scrapped.  
You may want to add a minimum date to scrapp from in the source code.

Gather every files with: 
`
cat *-*.txt >| user.data
`.    
To create a linkstream from it just use: 
`
python mail.py user.data linkstream.txt
`

# dependencies #

This script need :
* BeautifulSoup4
* request

It has been "tested" with python3.


Author
-------
* Author : Gaumont Noé
* Email    : noe.gaumont@lip6.fr
* Location : Paris, France

#### Version 0.1 ###

Disclaimer
-----------
If you find a bug, please send a bug report to noe.gaumont@lip6.fr
including if necessary the input file and the parameters that caused the bug.
You can also send me any comment or suggestion about the program.

License
--------

MIT
